#!/usr/bin/env python
# encoding: utf-8
"""
testServers.py

Created by Ramon Maria Gallart Escolà on 2012-02-04.
Copyright (c) 2012 __MyCompanyName__. All rights reserved.
"""

import sys
import thread
import time

from disposable.dumbservers import *
from disposable.dumbclients import *
from lib.utils import utils

def startudpserver(tid):
    print 'I\'m udp server with thread:', tid
    dumbUDPServer = DumbUDPServer()
    dumbUDPServer.listen()
    
def startudpclient():
    dumbUDPClient = DumbUDPClient()  
    dumbUDPClient.talk()
    
def main():
    # mirem si volem muntar un servidor TCP o UDP
    # el primer argument ha de ser udp o tcp
    try:
        arg = sys.argv[1]
    except:
        arg = ""

    if arg == 'udp':
        thread.start_new_thread(startudpserver, (1,))
        print 'Initializing connections...'
        time.sleep(1)
        startudpclient()
    elif  arg ==  'tcp':
        dumbTCPServer = DumbTCPServer()
        dumbTCPClient = DumbTCPClient()
    else:
        print 'Bad argument.'
        print 'Usage: %s [udp|tcp]' % sys.argv[0]

    sys.exit(0)


if __name__ == '__main__':
    main()

