#!/usr/bin/env python
# encoding: utf-8
"""
constants.py

Created by Ramon Maria Gallart Escolà on 2012-02-05.
Copyright (c) 2012 __MyCompanyName__. All rights reserved.
"""

# Public constants
PORT = 1060   # Default port to run the app
MAX = 65535   # Maximum data transmissions carried by an UDP datagram