#!/usr/bin/env python
# encoding: utf-8
"""
utils.py

Some general utilities

Created by Ramon Maria Gallart Escolà on 2012-02-04.
Copyright (c) 2012 __MyCompanyName__. All rights reserved.
"""

import random


def get_prime_number(upper=1000000):
    num = 2
    while True:
        num = get_random_number(1, upper)
        if MillerRabin(num):
            break
    return num


def get_random_number(lower=1, upper=10000):
    return random.randint(lower, upper)


def flip_coin():
    return get_random_number(0, 1)


def __toBinary(n):
    r = []
    while (n > 0):
        r.append(n % 2)
        n = n / 2
    return r


def __test(a, n):
    """
    test(a, n) -> bool Tests whether n is complex.

    Returns:
    - True, if n is complex.
    - False, if n is probably prime.
    """
    b = __toBinary(n - 1)
    d = 1
    for i in xrange(len(b) - 1, -1, -1):
        x = d
        d = (d * d) % n
        if d == 1 and x != 1 and x != n - 1:
            return True  # Complex
        if b[i] == 1:
            d = (d * a) % n

    if d != 1:
        return True  # Complex
    return False  # Prime


def MillerRabin(n, s=50):
    """
    Tests for the primality of the number n. If True is returned it is very
    probably a prime number. Otherwise i False is returned it is certainly
    not prime.

    This test has been taken and adapted from
    http://snippets.dzone.com/posts/show/4200

    Parameters:
    n - The number to test
    s - The number of tests. The chance that Rabin-Miller is mistaken about a
    number (i.e. thinks it's prime, but it's not) is 2^(-s). So, a value of 50
    for s is more than enough for any imaginable
    goal (2^(-50) is 8.8817841970012523e-16).

    Returns:
    True - if the number is prime (most probably)
    False - otherwise
    """
    for j in xrange(1, s + 1):
        a = random.randint(1, n - 1)
        if (__test(a, n)):
            return False  # n is complex
    return True  # n is prime
