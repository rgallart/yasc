#!/usr/bin/env python
# encoding: utf-8
"""
DumbServers.py

Created by Ramon Maria Gallart Escolà on 2012-02-05.
Copyright (c) 2012 __MyCompanyName__. All rights reserved.
"""

from base64 import b64encode
from base64 import b64decode
from lib.common import constants
from lib.utils import utils
import socket

class DumbUDPServer(object):
  def __init__(self):
    self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    self.group = 0
    self.prime = 0
    self.secret = 0
    self.key = 0
    self.public = 0
    self.public_client = 0
    
  def send_data(self, data, address):
    self.s.sendto(b64encode(data), address)
  
  def listen(self):
    """
    The server can receive two different messages:
      begin:  this message comes with the group, the prime and the public
              number of the client. The format for the message is:
                    begin#group#prime#client_public_number
              When the server receives this message it calcs its own public
              number which will be sent in response to this begin. The 
              server calculates the shared key too.
                    
      fin:    this message means the client has processed the data from the
              server and agrees with it. The format of the message is:
                    fin#
    The server sends two messages in response:
      nigeb:  this message contains the public number calculated by the 
              server. The format of the message is:
                    nigeb#server_public_number
              When the server sends this message waits until a fin message
              from the client arrives to set the state to 1
              
      nif:    This message tells the client the server is ready to start
              speaking. The format of the message is:
                    nif#
    All the data is sent and received encoded in base64
    """
    self.s.bind(('127.0.0.1', constants.PORT))
    print '[UDP Server] Listening at', self.s.getsockname()
    # state of the dialog with the client. When the server receives the
    # message 'fin' (without the quotes) state will be 1 and a 'nif'
    # (without the quotes) will be send to the client
    comm_ready = False
    while not comm_ready:
      data, address = self.s.recvfrom(constants.MAX)
      data = b64decode(data)
      data = data.split('#')
      print "[UDP Server] data received:", data
      if data[0] == 'begin':
        self.group = long(data[1])
        self.prime = long(data[2])
        self.public_client = long(data[3])
        self.secret = utils.get_random_number(1, 10000)
        self.public = (self.group**self.secret) % self.prime
        self.key = (self.public_client**self.secret) % self.prime
        print "[UDP Server] the key is:", self.key
        self.send_data("nigeb#" + str(self.public), address)
        pass
      elif data[0] == 'fin':
        self.send_data("nif#", address)
        state = True
        pass
      else:
        pass
      #data, address = self.s.recvfrom(constants.MAX)
      ## The data is base64 encoded
      #print "[UDP Server] Data is b64 encoded: %s" % data
      #data = b64decode(data)
      #data2 = data.split('#')
      #print '[UDP Server] The client at %s says [%s]' % (address, data)
      #if data2[0] == 'quit':
      #  self.send_data("%d bytes received" % len(data), address)
      #  break;
      #elif data2[0] == 'begin':
      #  self.send_data("A begin has been received", address)
      #elif data2[0] == 'number':
      #  self.send_data("A number has been received: %d" % int(data2[1]), address)
      #else:
      #  self.send_data("%d bytes received" % len(data), address)

class DumbTCPServer(object):
  def __init__(self):
    print 'this is DumbTCPServer!'
