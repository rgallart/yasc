#!/usr/bin/env python
# encoding: utf-8
"""
DumbClients.py

Created by Ramon Maria Gallart Escolà on 2012-02-05.
Copyright (c) 2012 __MyCompanyName__. All rights reserved.
"""

from base64 import b64encode
from base64 import b64decode
from lib.common import constants
from lib.utils import utils
import socket

class DumbTCPClient(object):
    def __init__(self):
        print 'this is DumbTCPClient!'    


class DumbUDPClient(object):
    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.group = 0
        self.prime = 0
        self.secret = 0
        self.key = 0
        self.public = 0
        self.public_server = 0
  
    def talk(self):
        """
        The client selects a group, a prime number and its own secret number.
        The group, the prime and public key of the client are passed to the
        server in order to calculate the key. The server will calculate it
        and select his own secret number. Once done, it will send its public
        key to the client. With all this information the client will be able
        to calculate the same key as the server.
        """
        self.secret = utils.get_random_number(1,10000)
        self.group = utils.get_random_number(1,1000)
        self.prime = utils.get_prime_number()
        self.public = (self.group**self.secret) % self.prime
        # sending the first message to the server
        self.s.sendto(b64encode("begin#" + str(self.group) + "#" + 
            str(self.prime) + "#" + str(self.public) ), 
              ('127.0.0.1', constants.PORT))
        # state of the dialog with the server. When the client receives the
        # message 'nif' (without the quotes) comm_ready will be True
        comm_ready = False
        while not comm_ready:
            data, address = self.s.recvfrom(constants.MAX)
            data = b64decode(data)
            data = data.split('#')
            print "[UDP Client] data received:", data
            if data[0] == "nigeb":
                self.public_server = long(data[1])
                self.key = (self.public_server**self.secret) % self.prime
                print "[UDP Client] the key is:", self.key
                self.s.sendto(b64encode("fin#"), ('127.0.0.1', constants.PORT))
                pass
            elif data[0] == "nif":
                comm_ready = True
            else:
                pass
