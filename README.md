YaSC(ko)'s README FILE
======================

1 - What is YaSC(ko)
--------------------
YaSC(ko) stands for "Yet another Secure Chat (kind of)".

2 - What is YaSC(ko) used for
-----------------------------
YaSC(ko) is aimed to provide a simple and reliabale enough method for secure chat. It's main purpose is not to be an incredible and powerful encrypted chat system. Instead it is focused to small chats between colleagues of the same classrom or office.

3 - How does it work
--------------------
YaSC(ko) works by default listening to port 4242, either UDP and TCP. It first tries to agree on a shared key that will be used all along the conversation in a way similar to the Diffie-Hellman algorithm. It works in a peer to peer way. The servers interchange UDP petitions used to negotiate the key. Then the clients connect each other via TCP using the established key to cipher the conversation. This way, no intermediate server is used to manage the conversation.
The conversation strings are converted to a byte array and then are ciphered bit per bit via XOR operation using the key. The receiving part uses the same XOR operation with the key agreed over the bytes sended. The result is the clear text of the message.

4 - What future features will be developed
------------------------------------------
By now the only way to work with YaSC(ko) is by establishing a one to one conversation between peers. It is my desire to make it grow and add a multichat functionality where the key for each participant will be shared.

Ramon Maria Gallart
